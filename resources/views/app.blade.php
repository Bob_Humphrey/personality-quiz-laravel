<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Personality Quiz</title>
    <link rel="stylesheet" href="/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
  </head>
  <body>

    <section id="content">
      @if (Request::path() != '/')
      <div id="page-heading">
        <h2>@yield('pageTitle')</h2>
      </div>
    @endif
    <div class="container @yield('pageClass')">
        @include('partials.alerts')
        @yield('content')
      </div>
    </section>
    @include('partials.footer')
    <script src="/js/all.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.14/vue.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
  </body>
</html>
